export const preloadTemplates = async function() {
	const templatePaths = [
		// Add paths to "modules/about-time/templates"
			'modules/about-time/templates/simpleCalendarDisplay.html',
			'modules/about-time/templates/countDown.html',
			'modules/about-time/tempaltes/calendarEditor.html'
	];

	return loadTemplates(templatePaths);
}
